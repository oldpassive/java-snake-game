package io.github.xyl;

import java.awt.*;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Random;

class SnakeGame {

    private static final int[][] dirs = new int[][]{{1, 0}, {-1, 0}, {0, -1}, {0, 1}};

    private final int width;

    private final int height;

    private final int timeLimit;

    private final Point[] food;

    private final LinkedList<Point> snake = new LinkedList<>();

    private final Deque<int[]> dirCache = new ArrayDeque<>();

    private int foodIdx = 0, score = 0;

    private int hp = 3;

    private int timePassed;

    public SnakeGame(int width, int height, int timeLimit) {
        this.width = width;
        this.height = height;
        this.timeLimit = timeLimit;
        food = new Point[15];
        for (int i = 0; i < 15; i++) {
            Random random = new Random();
            int randomX = random.nextInt(width);
            int randomY = random.nextInt(height);
            while (randomX == width / 2 && randomY == height / 2) {
                randomX = random.nextInt(width);
                randomY = random.nextInt(height);
            }
            food[i] = new Point(randomX, randomY);
        }
        snake.offerLast(new Point(width / 2, height / 2));
        dirCache.addLast(dirs[0]);
    }

    public LinkedList<Point> getSnake() {
        return snake;
    }

    public Point getFood() {
        return foodIdx < food.length ? food[foodIdx] : snake.peekFirst();
    }

    public int getScore() {
        return score;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void turn(int i) {
        if (dirCache.size() < 4) dirCache.offerLast(dirs[i]);
    }

    public boolean win() {
        return hp != 0 && timePassed <= timeLimit &&  score == food.length;
    }

    public boolean lose() {
        return hp == 0 || timePassed > timeLimit;
    }

    public void move() {
        Point curr = snake.peekFirst(), next = new Point();
        assert curr != null;
        int[] dir = dirCache.size() > 1 ? dirCache.pollFirst() : dirCache.peekFirst();
        assert dir != null;
        next.x = curr.x + dir[0];
        next.y = curr.y + dir[1];
        if (next.x < 0 || next.x == width || next.y < 0 || next.y == height) {
            hp--;
            return;
        }
        for (Point part : snake) {
            if (part.x == next.x && part.y == next.y) {
                hp--;
                return;
            }
        }
        if (foodIdx < food.length && next.x == food[foodIdx].x && next.y == food[foodIdx].y) {
            foodIdx++;
            score++;
        } else {
            snake.pollLast();
        }
        snake.offerFirst(next);
        timePassed++;
    }
}