package io.github.xyl;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameFrame extends JFrame {

    public GameFrame(SnakeGame snakeGame) {
        setName("Snake Game");
        setBounds(0, 0, snakeGame.getWidth() * 5 + 80, snakeGame.getHeight() * 5 + 100);
        setResizable(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel = new GamePanel(snakeGame);
        add(panel);
        setVisible(true);
        // Game loop to update the game every second
        Timer timer = new Timer(1000, e -> {
            snakeGame.move(); // Move the snake
            panel.repaint(); // Repaint the JPanel
            if (snakeGame.win() || snakeGame.lose()) {
                // Game over
                ((Timer) e.getSource()).stop();
                System.out.println("Score: " + snakeGame.getScore());
            }
        });
        // Add a KeyListener to the JFrame to handle keyboard input
        addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_ENTER -> timer.start(); // Start the timer
                    case KeyEvent.VK_UP -> snakeGame.turn(2); // Up
                    case KeyEvent.VK_RIGHT -> snakeGame.turn(0); // Right
                    case KeyEvent.VK_DOWN -> snakeGame.turn(3); // Down
                    case KeyEvent.VK_LEFT -> snakeGame.turn(1); // Left
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }
        });
    }
}
