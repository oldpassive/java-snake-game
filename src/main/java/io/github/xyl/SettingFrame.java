package io.github.xyl;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SettingFrame extends JFrame implements ActionListener {

    JTextField widthField, heightField, timeField;
    JButton submitButton, clearButton;

    public SettingFrame() {
        setTitle("Snake Game Setting");
        setVisible(true);
        setSize(1000, 1000);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        JLabel widthLabel = new JLabel("Width:");
        JLabel heightLabel = new JLabel("Height:");
        JLabel timeLabel = new JLabel("Time:");
        widthField = new JTextField();
        heightField = new JTextField();
        timeField = new JTextField();
        submitButton = new JButton("Submit");
        clearButton = new JButton("Clear");
        widthLabel.setBounds(80, 30, 200, 30);
        heightLabel.setBounds(80, 70, 200, 30);
        timeLabel.setBounds(80, 110, 200, 30);
        widthField.setBounds(300, 30, 200, 30);
        heightField.setBounds(300, 70, 200, 30);
        timeField.setBounds(300, 110, 200, 30);
        submitButton.setBounds(50, 160, 100, 30);
        clearButton.setBounds(170, 160, 100, 30);
        add(widthLabel);
        add(heightLabel);
        add(timeLabel);
        add(widthField);
        add(heightField);
        add(timeField);
        add(submitButton);
        add(clearButton);
        submitButton.addActionListener(this);
        clearButton.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == submitButton) {
            String width = widthField.getText();
            String height = heightField.getText();
            String time = timeField.getText();
            if (width.matches("\\d+") && height.matches("\\d+") && time.matches("\\d+")) {
                SnakeGame snakeGame = new SnakeGame(Integer.parseInt(width),
                        Integer.parseInt(height), Integer.parseInt(time) * 60);
                new GameFrame(snakeGame);
            } else {
                JOptionPane.showMessageDialog(this, "Invalid settings!");
            }
        } else {
            widthField.setText("");
            heightField.setText("");
            timeField.setText("");
        }
    }
}
