package io.github.xyl;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;

public class GamePanel extends JPanel {

    private final SnakeGame snakeGame;

    public GamePanel(SnakeGame snakeGame) {
        this.snakeGame = snakeGame;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.setBackground(Color.white);
        // Set the board
        this.setBounds(24, 30, snakeGame.getWidth() * 5, snakeGame.getHeight() * 5);
        // Draw the snake
        LinkedList<Point> body = snakeGame.getSnake();
        for (Point p : body) {
            g.fillRect(p.x * 5, p.y * 5, 5, 5);
        }
        // Draw the food
        Point food = snakeGame.getFood();
        g.fillRect(food.x * 5, food.y * 5, 5, 5);
    }
}
